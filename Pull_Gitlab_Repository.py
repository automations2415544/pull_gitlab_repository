import git
import os
from dotenv import load_dotenv

def pull_request(local_directory):
    repo = git.Repo(local_directory)

    try:
        origin = repo.remote()
        origin.pull()
        print("Pulled the latest changes.")
    except Exception as e:
        print(f"Failed to pull changes: {str(e)}")

load_dotenv()

# Local directory of the Git repository
local_directories = os.getenv("LOCAL_DIRECTORY").split(',')

for local_directory in local_directories:
    if local_directory:
        pull_request(local_directory)